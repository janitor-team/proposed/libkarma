#! /bin/sh

RIOCP=../tools/riocp
TESTDIR=`pwd`
HOME=`pwd`

rm -rf $HOME/.openrio
$RIOCP -a $TESTDIR/test1 > /dev/null 2>&1

test=test1
echo -n "$test: "
rm -f $HOME/.openrio/*/fileinfo
$RIOCP -ba $TESTDIR/$test > /dev/null 2>&1
[ -f $HOME/.openrio/*/fileinfo -a ! -s $HOME/.openrio/*/fileinfo ] \
	&& echo passed || echo failed

test=test2
echo -n "$test: "
rm -f $HOME/.openrio/*/fileinfo
$RIOCP -ba $TESTDIR/$test > /dev/null 2>&1
diff $HOME/.openrio/*/fileinfo $TESTDIR/$test/fileinfo \
	&& echo passed || echo failed

test=test3
echo -n "$test: "
mkdir -p $TESTDIR/$test/fids0 > /dev/null 2>&1
$RIOCP -a $TESTDIR/$test /dev/null > /dev/null 2>&1
[ -f $TESTDIR/$test/fids0/_00000/111 ] && echo passed || echo failed
rm -rf $TESTDIR/$test/*

test=test4
echo -n "$test: "
cp $TESTDIR/$test/var/smalldb.orig $TESTDIR/$test/var/smalldb
$RIOCP -bwa $TESTDIR/$test > /dev/null 2>&1
$RIOCP -bwa $TESTDIR/$test > /dev/null 2>&1
[ -s $HOME/.openrio/*/fileinfo ] \
	&& echo passed || echo failed

test=test5
echo -n "$test: "
cp $TESTDIR/$test/var/smalldb.orig $TESTDIR/$test/var/smalldb
$RIOCP -bwa $TESTDIR/$test > /dev/null 2>&1
diff $HOME/.openrio/*/fileinfo $TESTDIR/$test/fileinfo \
	&& echo passed || echo failed

