Summary:   	Rio Karma tools
Name:      	karma-tools
Version:   	0.1.2
Release:   	1.kb
License:   	GPL
Group:     	Development/Libraries
Url:	   	http://www.freakysoft.de/repos/libkarma
Source:   	http://www.freakysoft.de/libkarma/libkarma-%{version}.tar.gz
Packager:	Keith Bennett <keith@mcs.st-and.ac.uk>
BuildRoot: 	%{_tmppath}/%name-root

%description
Rio Karma tools

%package -n libkarma-devel
Summary:   	Rio Karma tools
Group:     	Development/Libraries

%description -n libkarma-devel
Rio Karma tools

%prep
%setup -n libkarma-%{version}

%build
make PREFIX=%_prefix

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
make install PREFIX=%_prefix

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc ChangeLog COPYING INSTALL THANKS TODO
%_bindir/riocp
%_bindir/chprop
%_bindir/playlist_show
%attr(4755,root,root) %_bindir/karma_helper

%files -n libkarma-devel
%defattr(-,root,root)
%_includedir/*
%_libdir/*
