#include <stdio.h>
#include <stdlib.h>
#include <lkarma.h>

int main(void){
    playlist * pl;
    uint32_t fid, fidg;
    uint32_t * tmp;
    int k,i;
    
    lk_properties_init();
    
    k=lk_karma_connect("10.0.0.42");
    lk_karma_authenticate(k, "rio");
    lk_karma_request_io_lock(k,IO_LOCK_R);
    
    lk_karma_load_database(k);
    lk_karma_hangup(k);
    lk_properties_save();
    
    tmp=lk_properties_andOrSearch(0, NULL, "type", "playlist");
    for(i=0;tmp[i]!=0;i++){
        pl=lk_playlist_fid_read(tmp[i]);
        printf("%i,%i,%i\n", tmp[i], lk_playlist_count(pl), pl->length);
        for(k=0;k<lk_playlist_count(pl);k++){
            lk_playlist_get_item(pl, k, &fid, &fidg);
            printf("playlist %s, %i, fid %u, fid_generation %u\n",
                   lk_playlist_get_name(pl), k, fid, fidg);
        }
        lk_playlist_free(&pl);
        
    }
    lk_properties_destroy();
    free(tmp);
    
    return 0;
}
