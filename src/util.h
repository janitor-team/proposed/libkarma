/*
 * libkarma/util.h
 *
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * char *simple_itoa was borrowed from boa [http://www.boa.org]
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 * 
 */

#ifndef _UTIL_H
#define _UTIL_H

char    *lk_path_string(char * file);
int      mk_path(const char *pathname);
uint32_t lk_htorl(uint32_t hostlong);
uint32_t lk_rtohl(uint32_t hostlong);
uint64_t lk_htorll(uint64_t hostlong);
uint64_t lk_rtohll(uint64_t hostlong);
unsigned char *lk_generate_rid(int file, int offset, int length);
int      lk_is_karma_mountpoint(const char *mountpoint);

#endif /* _UTIL_H */
