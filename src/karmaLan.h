/*
 * libkarma/karma.h
 *
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *
 */

#ifndef _KARMA_LAN_H
#define _KARMA_LAN_H

#include <inttypes.h>

#define RIOPORT 8302
#define HEADERLENGTH 8
#define SALTLENGTH 16
#define DETAILSLENGTH 32

#define RIO_HEADER "\x52\x69\xC5\x8D" /* Ri~o */

#define GET_PROTOCOL_VERSION    0
#define NAK                     1
#define BUSY                    2
#define GET_AUTHENTICATION_SALT 3
#define AUTHENTICATE            4
#define GET_DEVICE_DETAILS      5
#define GET_STORAGE_DETAILS     6
#define GET_DEVICE_SETTINGS     7
#define UPDATE_DEVICE_SETTINGS  8
#define REQUEST_IO_LOCK         9
#define RELEASE_IO_LOCK        10
#define PREPARE                11
#define WRITE_FILE_CHUNK       12
#define GET_ALL_FILE_DETAILS   13
#define GET_FILE_DETAILS       14
#define UPDATE_FILE_DETAILS    15
#define READ_FILE_CHUNK        16
#define DELETE_FILE            17
#define FORMAT_STORAGE         18
#define HANGUP                 19
#define DEVICE_OPERATION       20

char *lk_karmaLan_fidToPath(int rio, uint32_t file_id);

/*Help-Functions:*/
int      lk_karmaLan_connect                (char *ip);
int      lk_karmaLan_send_request           (int rio, uint32_t identifier,
                                             char *payload, int plen);

/*Basic Protocol Functions*/
int      lk_karmaLan_get_protocol_version   (int rio, uint32_t *major_version,
                                             uint32_t *minor_version);
int32_t  lk_karmaLan_get_authentication_salt(int rio, char **salt);
uint32_t lk_karmaLan_authenticate           (int rio, char *pass);
int      lk_karmaLan_get_device_details     (int rio, char **name,
                                             char **version,
                                             uint32_t *storagedevices);
int      lk_karmaLan_get_storage_details    (int rio, uint32_t storage_id,
                                             uint32_t *n_files,
                                             uint64_t *s_size,
                                             uint64_t *f_space,
                                             uint32_t *highest_file_id);
int      lk_karmaLan_get_device_settings    (int rio);
int32_t  lk_karmaLan_update_device_settings (int rio, char *properties);
int32_t  lk_karmaLan_request_io_lock        (int rio, uint32_t type);
int32_t  lk_karmaLan_release_io_lock        (int rio);
int32_t  lk_karmaLan_prepare                (int rio, uint64_t size,
                                             uint32_t *file_id,
                                             uint32_t storage_id);
int32_t  lk_karmaLan_write_file_chunk       (int rio, uint64_t offset,
                                             uint64_t size, uint32_t file_id,
                                             uint32_t storage_id,
                                             const char *data);
int32_t  lk_karmaLan_get_all_file_details   (int rio, char **properties);
int32_t  lk_karmaLan_get_file_details       (int rio, uint32_t file_id,
                                             char **properties);
int32_t  lk_karmaLan_update_file_details    (int rio, uint32_t file_id,
                                             char *properties);
int32_t  lk_karmaLan_read_file_chunk        (int rio, uint64_t offset,
                                             uint64_t size, uint32_t file_id,
                                             char **data,  uint64_t *retsize);
int32_t  lk_karmaLan_delete_file            (int rio, uint32_t file_id);
int32_t  lk_karmaLan_format_storage         (int rio, uint32_t storage_id);
int32_t  lk_karmaLan_hangup                 (int rio);
int32_t  lk_karmaLan_device_operation       (int rio, uint64_t size,
                                             char *data, char **newdata);
int      lk_karmaLan_write_smalldb          (void);

/*Advanced Protocol Functions:*/
void     lk_karmaLan_load_database          (int rio);
void     lk_karmaLan_update_database        (int rio);

#endif /* _KARMA_LAN_H */
