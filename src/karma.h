/*
 * libkarma/karma.h
 *
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *
 */

#ifndef _KARMA_H
#define _KARMA_H

#include <inttypes.h>

extern int using_usb;
extern char *karma_tmpdir;

char *   lk_karma_fidToPath              (int rio, uint32_t file_id);

/*Help-Functions:*/
int      lk_karma_send_request           (int rio, uint32_t identifier,
                                          char *payload, int plen);

/*Basic Protocol Functions*/
int      lk_karma_get_protocol_version   (int rio, uint32_t *major_version,
                                          uint32_t *minor_version);
int32_t  lk_karma_get_authentication_salt(int rio, char **salt);
int      lk_karma_get_device_details     (int rio, char **name, char **version,
                                          uint32_t *storagedevices);
int32_t  lk_karma_update_device_settings (int rio, char *properties);
int32_t  lk_karma_prepare                (int rio, uint64_t size,
                                          uint32_t *file_id,
                                          uint32_t storage_id);
int32_t  lk_karma_format_storage         (int rio, uint32_t storage_id);
int32_t  lk_karma_device_operation       (int rio, uint64_t size, char *data,
                                          char **newdata);
/*Advanced Protocol Functions:*/
void     lk_karma_update_database        (int rio);

int      lk_karma_parse_settings         (char *buf);

/* private stuff */
struct lk_ops
{
    int      (*lk_karma_connect)                (char *ipHostOrPath);
    int32_t  (*lk_karma_hangup)                 (int rio);
    int      (*lk_karma_send_request)           (int rio, uint32_t identifier,
                                                 char *payload, int plen);
    int      (*lk_karma_get_protocol_version)   (int rio,
                                                 uint32_t *major_version,
                                                 uint32_t *minor_version);
    int32_t  (*lk_karma_get_authentication_salt)(int rio, char **salt);
    uint32_t (*lk_karma_authenticate)           (int rio, char *pass);
    int      (*lk_karma_get_device_details)     (int rio, char **name,
                                                 char **version,
                                                 uint32_t * storagedevices);
    int      (*lk_karma_get_storage_details)    (int rio, uint32_t storage_id,
                                                 uint32_t * n_files,
                                                 uint64_t * s_size,
                                                 uint64_t * f_space,
                                                 uint32_t * highest_file_id);
    int      (*lk_karma_get_device_settings)    (int rio);
    int32_t  (*lk_karma_update_device_settings) (int rio, char *properties);
    int32_t  (*lk_karma_request_io_lock)        (int rio, uint32_t type);
    int32_t  (*lk_karma_release_io_lock)        (int rio);
    int32_t  (*lk_karma_prepare)                (int rio, uint64_t size,
                                                 uint32_t * file_id,
                                                 uint32_t storage_id);
    int32_t  (*lk_karma_get_all_file_details)   (int rio, char **properties);
    int32_t  (*lk_karma_get_file_details)       (int rio, uint32_t file_id,
                                                 char **properties);
    int32_t  (*lk_karma_update_file_details)    (int rio, uint32_t file_id,
                                                 char *properties);
    int32_t  (*lk_karma_read_file_chunk)        (int rio, uint64_t offset,
                                                 uint64_t size,
                                                 uint32_t file_id, char **data,
                                                 uint64_t * retsize);
    int32_t  (*lk_karma_write_file_chunk)       (int rio, uint64_t offset,
                                                 uint64_t size,
                                                 uint32_t file_id,
                                                 uint32_t storage_id,
                                                 const char *data);
    int32_t  (*lk_karma_delete_file)            (int rio, uint32_t file_id);
    int32_t  (*lk_karma_format_storage)         (int rio, uint32_t storage_id);
    int32_t  (*lk_karma_device_operation)       (int rio, uint64_t size,
                                                 char *data, char **newdata);
    void     (*lk_karma_load_database)          (int rio);
    void     (*lk_karma_update_database)        (int rio);
    int      (*lk_karma_write_smalldb)          (void);
    char *   (*lk_karma_fidToPath)              (int rio, uint32_t file_id);
};

#endif /* _KARMA_H */
