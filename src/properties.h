/*
 * libkarma/properties.h
 * 
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 * Copyright (c) Keith Bennett <keith@mcs.st-and.ac.uk>
 * 
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 * 
 */

#ifndef _PROPERTIES_H
#define _PROPERTIES_H

#include <iconv.h>
#include <inttypes.h>
#include <stdio.h>

#include "hash.h"
#include "lkarma.h"

#define RK_SMALLDB "/var/smalldb"

typedef enum smalldb_type
{
    SDB_LE32,
    SDB_STRING,
    SDB_BLOB,
    SDB_CHAR,
    SDB_LE16
} smalldb_type_t;

typedef struct smalldb_attrib
{
    char *name;
    smalldb_type_t type;
    uint32_t dunno;
} smalldb_attrib_t;

typedef struct playlist_attrib
{
    uint32_t index;
    uint32_t len;
    char *playlist;
} playlist_t;

typedef struct karma_db
{
    uint32_t version;
    uint32_t ctime;
    uint32_t eight;
    uint32_t three;
    uint32_t num_attribs;
    uint32_t prop_size;
    uint32_t num_playlists;
    uint32_t dmap_size;
    smalldb_attrib_t *attribs;
    playlist_t *playlists;
    unsigned char *dmap;
} karma_db_t;

typedef struct table
{
    char *str;
    uint32_t len;
    uint32_t idx;
} table_t;

extern uint32_t device_generation;
extern uint32_t serial;
extern int properties_updated;

/*
char *   lk_properties_get_codeset      (void);
void     lk_properties_set_codeset      (char * codeset);
*/

uint32_t lk_properties_getnextfid       (void);

char *   lk_properties_get_property_hash(HASH * p, char * key);
void     lk_properties_set_property_hash(HASH * p, char * property,
                                         char * data);

HASH *   lk_properties_idsearch         (uint32_t fid);

void     lk_properties_inc_devgeneration(void);

int      lk_properties_write_smalldb    (char *usbMountPoint, karma_db_t *db);
int      lk_properties_write_property   (FILE *fp, char *attr,
                                         smalldb_type_t type, table_t *table,
                                         uint32_t *offset, uint32_t *arrsz,
                                         char **arr);

#endif /* _PROPERTIES_H */
