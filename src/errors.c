/*
 * libkarma/errors.c
 *
 * Copyright (c) Enrique Vidal 2004
 *           (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "lkarma.h"

const char *lkerrorList[MAXLKERRORS + 1] = {
    /* E_NOERR      0 */ "",
    /* E_HOST       1 */ "** Libkarma error: gethostbyname() failed",
    /* E_SOCKET     2 */ "** Libkarma error: socket() failed",
    /* E_SENDTO     3 */ "** Libkarma error: sendto() failed",
    /* E_CONNECT    4 */ "** Libkarma error: connect() failed",
    /* E_RECVFROM   5 */ "** Libkarma error: recvfrom() failed",
    /* E_SELECT     6 */ "** Libkarma error: select() failed",
    /* E_OPEN       7 */ "** Libkarma error: open() failed",
    /* E_ICONVO     8 */ "* Libkarma warning: iconv_open() failed",
    /* E_ICONV      9 */ "* Libkarma warning: iconv() failed",
    /* E_ICONVC    10 */ "** Libkarma error: iconv_close() failed",
    /* E_BADHEADER 11 */ "** Libkarma error: broken Rio header",
    /* E_BADIDENT  12 */ "** Libkarma error: wrong protocol identifier",
    /* E_FAILEDREQ 13 */ "* Libkarma warning: failed request",
    /* E_BADPROP   14 */ "** Libkarma error: property parsing failed",
    /* E_BADID     15 */ "** Libkarma error: given id not found",
    /* E_NOSSDP    16 */ "* Libkarma warning: no ssdp answer",
    /* E_SSDPPARSE 17 */ "* Libkarma warning: ssdp parsing failed",
    /* E_MKDIR     18 */ "** Libkarma error: mkdir() failed",
    /* E_UTIME     19 */ "** Libkarma error: utime() failed",
    /* E_NODIR     20 */ "** Libkarma error: unexisting directory",
    /* E_NOPROP    21 */ "* Libkarma warning: missing properties file",
    /* E_READ      22 */ "* Libkarma warning: read error",
    /* E_WRITE     23 */ "** Libkarma error: write error",
    /* E_DELETE    24 */ "** Libkarma error: delete error",
    /* E_NODEVSET  25 */ "* Libkarma warning: no device-settings file",
    /* E_UNIMPLEM  26 */ "* Libkarma warning: unimplemented call",
    /* E_MFINDERR  27 */ "* Libkarma warning: mount search error",
    /* E_NOMOUNT   28 */ "* Libkarma warning: no mountpoint found",
    /* E_MANYMOUNT 29 */ "* Libkarma warning: more than one mountpoints",
    /* E_NOSMALLDB 30 */ "* Libkarma warning: Missing RK index file (smalldb)",
    /* E_DUPE      31 */ "* Libkarma warning: tune already present",
    /* E_PATHCREAT 32 */ "* Libkarma warning: dir access/create failed",
    /* E_NOHASH    33 */ "** Libkarma error: no hash found",
    /* E_WRCHUNK   34 */ "** Libkarma error: write_file_chunk() failed",
    /* E_SMALLMP3  35 */ "** Libkarma error: tune file is too small",
    /* E_UNSUPTAG  36 */ "* Libkarma warning: unsupported tag type",
    /* E_NOTAGFILE 37 */ "* Libkarma warning: can't access tags file",
    /* E_BADFDB    38 */ "* Libkarma warning: unrecognised fdb file",
    /* E_UNSUPFDB  39 */ "* Libkarma warning: unsupported fdb file",
    /* E_NOPATHPR  40 */ "* Libkarma warning: missing path properties",
    /* E_NOTMPDIR  41 */ "* Libkarma warning: no temporary directory found",
    /* E_TMPCREAT  42 */ "** Libkarma error: can't create temporary tag file"
};

#define libkarmaErrorString lkerrorList[libkarmaError]
#define previousErrorString lkerrorList[previousError]

/* libkarmaErrorString is assumedly valid only if libkarmaErrorFlag == 1 */
static int libkarmaError = 0;
static int previousError = 0;
static int autoPrintErrors = 1;
static error_handler *callback = NULL;

/* ---------------------------- Error Setting --------------------------- */
int lk_errors_set(const int err)
{
    lk_errors_number();
    if((err < 0) || (err > MAXLKERRORS))
        return -1;
    libkarmaError = err;
    if(callback) {
        callback();
    }
    if(autoPrintErrors)
        fprintf(stderr, "%s\n", libkarmaErrorString);
    return 0;
}

/* -------------------------- Error checking -------------------------- */
void lk_errors_autoprint(const int autoPrint)
{
    autoPrintErrors = autoPrint;
}

void lk_errors_setHandler(error_handler * handler)
{
    callback = handler;
}

int lk_errors_number()
{
    /* checks current error number and backs-up and resets it */
    previousError = libkarmaError;
    libkarmaError = 0;
    return previousError;
}

int lk_errors_number_noreset()
{
    return libkarmaError;
}

int lk_errors_p(const char *s1, const char *s2)
{
    if(lk_errors_number())
        fprintf(stderr, "%s%s%s\n", s1, previousErrorString, s2);
    return previousError;
}

char *lk_errors_numberstr(int lkerrnum)
{
    if((lkerrnum < 0) || (lkerrnum > MAXLKERRORS))
        return NULL;
    return (char *)lkerrorList[lkerrnum];
}
