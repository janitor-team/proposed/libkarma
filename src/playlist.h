/*
 * libkarma/playlist.h
 *
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *
 */

#ifndef _PLAYLIST_H
#define _PLAYLIST_H

#include <inttypes.h>
#include "lkarma.h"

char *       lk_playlist_escape          (char * data, unsigned int length);
unsigned int lk_playlist_unescape_inplace(char * str);
unsigned int lk_playlist_unescape        (char * str, char ** data);

int          lk_playlist_delete          (playlist * pl, int karma);
int          lk_playlist_remove          (playlist * pl, unsigned int n);
int          lk_playlist_insert          (playlist * pl, unsigned int n,
                                          uint32_t fid,
                                          uint32_t fid_generation);

#endif /* _PLAYLIST_H */
