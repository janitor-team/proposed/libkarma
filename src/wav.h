/*
 * libkarma/wav.h
 *
 * Copyright (c) Paul van der Mark <pmark@liacs.nl> ???? *
 *                            (playwav.c - ask the search engine of your choice)
 *           (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 * 
 */

#ifndef _WAV_H
#define _WAV_H

typedef struct {
    char            RiffID [4] ;
    u_long          RiffSize ;
    char            WaveID [4] ;
    char            FmtID  [4] ;
    u_long          FmtSize ;
    u_short         wFormatTag ;
    u_short         nChannels ;
    u_long          nSamplesPerSec ;
    u_long          nAvgBytesPerSec ;
    u_short         nBlockAlign ;
    u_short         wBitsPerSample ;
} wave_header;

int openwav(wave_header **, char *);
void cleanup(wave_header **);

#endif /* _WAV_H */
