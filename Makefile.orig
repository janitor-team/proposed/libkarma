LK_SRC=src
TOOLSDIR=tools
PREFIX=/usr
INSTALL?=./install-sh
VERSION=0.1.1
SHELL=/bin/bash

export PREFIX
export VERSION

DISTNAME=libkarma-$(VERSION)
DISTFILES= $(LK_SRC)/{Jamfile,Makefile,*[ch]} \
	$(TOOLSDIR)/{Jamfile,Makefile,*[ch]} \
	karma-sharp/{Makefile,karma-sharp.pc.in,Song.cs,Device.cs} \
	Jamfile Makefile COPYING ChangeLog THANKS INSTALL* TODO \
	install-sh karma-tools.spec

INSTALL_DIR     := $(INSTALL) -d -o root -g root -m 0755
INSTALL_FILE    := $(INSTALL)    -o root -g root -m 0644
INSTALL_PROGRAM := $(INSTALL)    -o root -g root -m 0755
INSTALL_SCRIPT  := $(INSTALL)    -o root -g root -m 0755

all: libkarma tools karma-sharp

static: libkarma toolsStatic karma-sharp

install: libkarma tools karma-sharp
	cd $(LK_SRC) && $(MAKE) install
	cd karma-sharp && $(MAKE) install
	cd tools && $(MAKE) install

uninstall: 
	cd $(LK_SRC) && $(MAKE) uninstall
	cd karma-sharp && $(MAKE) uninstall
	cd tools && $(MAKE) uninstall

libkarma: FORCE
	cd $(LK_SRC) && $(MAKE)

tools: FORCE
	cd $(TOOLSDIR) && $(MAKE)

toolsStatic: FORCE
	cd $(TOOLSDIR) && $(MAKE) static

karma-sharp: FORCE
	cd karma-sharp && $(MAKE)

dist: 
	mkdir -p $(DISTNAME)
	mkdir -p $(DISTNAME)/$(LK_SRC)
	mkdir -p $(DISTNAME)/$(TOOLSDIR)
	mkdir -p $(DISTNAME)/karma-sharp
	cp --parents $(DISTFILES) $(DISTNAME)
	tar czvf $(DISTNAME).tar.gz $(DISTNAME)
	$(RM) -r $(DISTNAME)

distcheck: dist
	mkdir build
	cd build && tar xzvf ../$(DISTNAME).tar.gz && \
	cd $(DISTNAME) && $(MAKE)
	$(RM) -r build

.PHONY: clean

clean-libkarma:
	( cd $(LK_SRC)    ; make clean )

clean-tools:
	( cd $(TOOLSDIR)  ; make clean )

clean-karma-sharp:
	( cd karma-sharp  ; make clean )

clean: clean-tools clean-libkarma clean-karma-sharp

FORCE:
